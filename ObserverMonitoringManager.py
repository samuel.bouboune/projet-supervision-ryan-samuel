from Monitoring_module.Monitoring_Manager import Monitoring_Manager
from Hardware import Hardware
from ObservableUserInterface import ObservableUserInterface

ADDED_HARDWARE = 0
MODIFIED_HARDWARE = 1
DELETED_HARDWARE = 2

class ObserverMonitoringManager(Monitoring_Manager):
    def __init__(self, monitored_hardware: list[Hardware]):
        super().__init__(monitored_hardware)

    def notify(self, operation, oldhardware, newhardware):
        """
        En fonction du changement qu'il y a eu dans la configuration, on crée, supprime, modifie un thread
        """
        if operation == ADDED_HARDWARE:
            self.addhardware(newhardware)
        elif operation == MODIFIED_HARDWARE:
            self.modifyhardware(oldhardware, newhardware)
        elif operation == DELETED_HARDWARE:
            self.removehardware(oldhardware)
