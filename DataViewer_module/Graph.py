import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from datetime import datetime


class Graph():
    """Classe qui représente un graphique d'un OID à afficher"""
    def __init__(self, path):
        """Initialise un premier affichage du graphe"""
        self.path = path
        data = pd.read_csv(path)
        x  = data['Timestamp'].map(lambda x: datetime.strptime(str(x), '%d/%m/%Y %H:%M:%S'))
        y1 = data['Valeur']
        description = data['Description'][0]
        self.line = plt.plot(x, y1, label=description)
        plt.xticks(rotation=90)
        plt.legend(loc='upper left')
        plt.tight_layout()

    @staticmethod
    def __animate(i, line, path):
        """Méthode appelée régulièrement pour actualiser les données"""
        line = line[0] #Pour une raison que j'ignore, l'objet line est ajouté dans une liste, il faut l'en sortir
        data = pd.read_csv(path)
        x  = data['Timestamp'].map(lambda x: datetime.strptime(str(x), '%d/%m/%Y %H:%M:%S'))
        y1 = data['Valeur']
        line.set_data(x,y1)
        plt.xticks(rotation=90)
        plt.tight_layout()
        
    def run(self):
        """ La classe Funcanimation appelée en dessous permet de créer l'animation du graphe
        Il faut lui passer en paramètre un pointeur vers la fonction animate
        et les arguments requis. Elle va ensuite boucler pour afficher les nouvelles data"""
        args=(self.line, self.path)
        self.ani = FuncAnimation(plt.gcf(), Graph.__animate, interval=1000, fargs=args)

if __name__ == "__main__":
    graphe = Graph('data/192.168.176.2/1.3.6.1.4.1.2021.10.1.5.1.csv')
    graphe.run()
    plt.tight_layout()
    plt.show()
    