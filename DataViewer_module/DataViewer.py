from DataViewer_module.Graph import Graph
import matplotlib.pyplot as plt
import os, sys
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

from Hardware import Hardware

class DataViewer:
    """Objet qui fournit une interface utilisateur pour afficher les graphes"""
    def __init__(self, monitored_hardware: list[Hardware]) -> None:
        self.monitored_hardware = monitored_hardware

    def interface(self):
        """Interface principale"""
        chosen_hardware = self.ask_user_hardware()
        path_to_oid_data = self.ask_user_oid(chosen_hardware)
        self.display_data(path_to_oid_data)

    def display_data(self, path_to_oid:str) -> None:
        """ Créer un objet graphe pour afficher l'oid d'un matériel"""
        graphe = Graph(path_to_oid)
        graphe.run()
        plt.tight_layout()
        plt.show()
        

    def ask_user_hardware(self) -> Hardware:
        """Demande à l'utilisateur le matériel dont il veut visualiser un oid"""
        print("Liste de matériels : ")
        for hardware in self.monitored_hardware:
            print(hardware)
        ip_chosen_hardware = str(input("\nChoisir un matériel dont vous voulez afficher " + 
                    "les données en tapant son adresse IP : "))
        chosen_hardware = self.getHardware(ip_chosen_hardware)
        return chosen_hardware
    
    def ask_user_oid(self, chosen_hardware: Hardware) -> str:
        """Demande à l'utilisateur l'oid qu'il veut visualiser sur le matériel qu'il a choisi"""
        print("Voici la liste d'oid du matériel que vous avez choisi : ")
        print(chosen_hardware.getOids())

        oid_invalide = True 
        while oid_invalide:
            oid = str(input("Taper l'oid à visualiser sans erreur : "))
            for oiddict in chosen_hardware.oid_dict_list:
                if oiddict['oid'] == oid:
                    oid_invalide = False

        folder = "data/" + chosen_hardware.ip_address + "/"
        datafilename = folder + oid + ".csv"

        return datafilename


    def getHardware(self, ip_address: str) -> Hardware:
        """ Renvoie l'objet Hardware correspondant à l'adresse ip indiquée"""
        hardware_list = self.monitored_hardware
        for hardware in hardware_list:
            if(hardware.ip_address == ip_address):
                return hardware
        return None

class DataViewerV2(DataViewer):
    """Cette version permet d'améliorer le choix du matériel"""
    def __init__(self, monitored_hardware: list[Hardware]) -> None:
        super().__init__(monitored_hardware)
    
    def ask_user_hardware(self) -> Hardware:
        """Demande à l'utilisateur le matériel dont il veut visualiser un oid"""
        print("Liste de matériels : ")
        for hardware in self.monitored_hardware:
            print("Numéro de matériel : " + str(self.monitored_hardware.index(hardware)))
            print(hardware)

        num_invalide = True 
        while num_invalide:
            num = int(input("Taper le numéro de matériel à visualiser : "))
            num_invalide = not((num >= 0) and (num < len(self.monitored_hardware)))
        chosen_hardware = self.monitored_hardware[num]
        return chosen_hardware
    
    def ask_user_oid(self, chosen_hardware: Hardware) -> str:
        """Demande à l'utilisateur l'oid qu'il veut visualiser sur le matériel qu'il a choisi"""
        print("Voici la liste d'oid du matériel que vous avez choisi : ")
        oidlist = chosen_hardware.getOids()
        for oid in oidlist:
            print("Numéro d'OID : " + str(oidlist.index(oid)))
            print(oid)
        
        num_invalide = True 
        while num_invalide:
            num = int(input("Taper l'oid à visualiser sans erreur : "))
            num_invalide = not((num >= 0) and (num < len(oidlist)))

        folder = "data/" + chosen_hardware.ip_address + "/"
        datafilename = folder + oidlist[num]["oid"] + ".csv"
        return datafilename

if __name__ == "__main__":
    from ObservableUserInterface import ObservableUserInterface
    #Création du config manager 
    uiconfigmanager = ObservableUserInterface()
    
    #Création du Monitoring_Manager
    hardware_list = uiconfigmanager.getMonitoredHardware() #ATTENTION : passage par référence !!
    dataviewer = DataViewerV2(hardware_list)
    dataviewer.interface()

    


