from Configuration_module.UserInterface import UserInterface
from Hardware import Hardware

ADDED_HARDWARE = 0
MODIFIED_HARDWARE = 1
DELETED_HARDWARE = 2

class ObservableUserInterface(UserInterface):
    def __init__(self):
        super().__init__()
        self.observers = list()
    
    def registerObserver(self,observer):
        self.observers.append(observer)
    
    def notifyObservers(self, operation, oldhardware, newhardware):
        for observer in self.observers:
            observer.notify(operation, oldhardware, newhardware)
    
    def addHardware(self):
        """Demande à l'utilisateur d'ajouter un matériel et l'ajoute dans la configuration
        Prévient les observateurs d'un ajout"""
        added_hardware = super().addHardware()
        oldhardware = None
        if added_hardware:
            self.notifyObservers(ADDED_HARDWARE, oldhardware, added_hardware)
    
    def delHardware(self):
        """Demande à l'utilisateur de supprimer un matériel et l'ajoute dans la configuration
        Prévient les observateurs d'une suppression"""
        deleted_hardware = super().delHardware()
        new_hardware = None
        if deleted_hardware:
            self.notifyObservers(DELETED_HARDWARE, deleted_hardware, new_hardware)
    
    def modifyHardware(self):
        """Demande à l'utilisateur de modifier un matériel et l'ajoute dans la configuration
        Prévient les observateurs d'une modification"""
        oldhardware, newhardware = super().modifyHardware()
        if oldhardware and newhardware:
            self.notifyObservers(MODIFIED_HARDWARE, oldhardware, newhardware)