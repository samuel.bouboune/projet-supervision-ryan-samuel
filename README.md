# Projet Supervision - Ryan Samuel

## Installation de l'application
Prérequis : 
	- Avoir Python d'installé 

	- Avoir pip pour gérer les paquets python. En principe pip est installé par défaut avec python3 mais peut ne pas être dans les variables d'environnement système.



Installer les dépendances : 
	Commande à taper depuis un terminal dans le dossier du projet : "pip install -r requirements.txt" ou "python -m pip install -r requirements.txt"

Une fois les dépendances installées, lancer application.py.

