"""
Auteur : Ryan BADAÏ

Prérequis : 
Aucun

Description :
Cette classe Hardware sert à modéliser un matériel que l'on va monitorer 
"""

class Hardware:
    # def __init__(self, string, string, dict(), string, int, string)
    def __init__(self, ip_address: str, name: str, oid_dict_list: list[dict], community: str):
        """args : oid_dict_list : contient une liste de dictionnaires
        Chaque dictionnaire contient trois clés, description, oid, et req_time, ex:
        oid_dict_list[0]["oid"]
        oid_dict_list[0]["description"]
        oid_dict_list[0]["req_time"] 
        """
        self.ip_address = ip_address
        self.name = name 
        self.community = community
        self.oid_dict_list = oid_dict_list
       

    def getIPAaddress(self):
        return self.ip_address

    def getName(self):
        return self.name
    
    def getOids(self):
        return self.oid_dict_list
    
    def getCommunity(self):
        return self.community
    
    def setIPAddress(self,ip_address):
        self.ip_address = ip_address

    def setName(self,name):
        self.name = name 
    
    def setOidList(self, oid_dict_list):
        self.oid_dict_list = oid_dict_list
        
    def setCommunity(self, community):
        self.community = community
    
    def __eq__(self, other) -> bool:
        """Surcharge de l'opérateur == """
        assert(isinstance(other,Hardware))
        if self.ip_address == other.ip_address and self.oid_dict_list == other.oid_dict_list and self.name == other.name and self.community == other.community:
            return True
        else:
            return False
    
    def __repr__(self):
        str= "Nom :" + self.name + " | Adresse IP : " + self.ip_address + " | Communauté : " + self.community
        for oiddict in self.oid_dict_list:
            str += "\n" + oiddict.__repr__()
        str+="\n"
        return str


class ModifyHardware(Hardware):
    """Classe fournissant une interface utilisateur pour la modification du matériel """
    def __init__(self, hard: Hardware):
        super().__init__(hard.ip_address, hard.name, hard.oid_dict_list, hard.community)
    
    def modifyIPAddress(self):
        from Configuration_module.UserInterface import UserInterface
        print("Adresse IP actuelle : " + self.ip_address)
        ip_addr = str(input("Saisissez une nouvelle adresse IP ou q pour quitter : "))
        while not UserInterface.testformatip(ip_addr) or not "q":
            ip_addr = str(input("Adresse IP incorrecte ! Recommencer ou q pour quitter : "))

        if ip_addr == "q":
            return False

        self.setIPAddress(ip_addr)
        print("Adresse IP modifiée avec succès")
        return True
    
    def modifyCommunity(self):
        print("Communauté actuelle : " + self.community)
        community = str(input("Saisissez une nouvelle communauté ou q pour quitter : "))
        if community == "q":
            return False
        self.setCommunity(community)
        print("Communauté modifiée avec succès")
        return True
    
    def modifyName(self):
        print("Nom actuel : " + self.name)
        name = str(input("Saisissez un nouveau nom ou q pour quitter : "))
        if name == "q":
            return False
        self.setName(name)
        print("Nom modifié avec succès")
        return True

    def modifyOidListDict(self):
        from Configuration_module.UserInterface import UserInterface
        notfinished = True
        while notfinished:
            self.print_oid_list()
            config_option = str(input("\nSaisir le caractère correspondant pour effectuer les actions suivantes" +
                                                "\n\"1\" ==> Modifier un OID existant" +
                                                "\n\"2\" ==> Ajouter un OID" +
                                                "\n\"3\" ==> Supprimer un OID" +
                                                "\n\"r\" ==> Retour" +
                                                "\n\n                                    Choix: "))
            if config_option == "1" :
                self.modoiddict()
            if config_option == "2" :
                self.addoiddict()
            if config_option == "3" :
                self.rmoiddict()
            if config_option.lower() == "r" :
                notfinished = False
                break
        
    def modoiddict(self):
        from Configuration_module.UserInterface import UserInterface
        if len(self.oid_dict_list) == 0:
            print("Pas d'OID à modifier, merci d'en ajouter !")
            return 1
        self.print_oid_list()
        oidamodifier= int(input("Choisissez le numéro d'OID à modifier ou q pour quitter : "))

        while oidamodifier < 0 or oidamodifier > len(self.oid_dict_list) - 1 or not isinstance(oidamodifier,int):
            oidamodifier=input("Mauvais numéro ! Recommencer ou q pour quitter : ")
        
        if oidamodifier == "q":
            return 1
            
        oidamodifier = int(oidamodifier)
        new_oid_dict=UserInterface.ask_oid_dict()
        self.oid_dict_list[oidamodifier] = new_oid_dict
        print("OID modifié avec succès")
        return 0

    def addoiddict(self):
        from Configuration_module.UserInterface import UserInterface
        self.print_oid_list()
        oiddict = UserInterface.ask_oid_dict()
        self.oid_dict_list.append(oiddict)
        print("OID ajouté !")
    
    def rmoiddict(self):
        self.print_oid_list()
        oid = str(input("Entrez un oid : "))
        oiddict = self.get_oid_dict(oid)
        self.oid_dict_list.remove(oiddict)
        print("OID supprimé !")
    
    def get_oid_dict(self,oid: str) -> dict:
        for element in self.oid_dict_list:
            if element["oid"] == oid:
                return element
        return None

    def print_oid_list(self):
        print("Oid actuels : ")
        for oiddict in self.oid_dict_list:
            print("OID n° : " + str(self.oid_dict_list.index(oiddict)))
            print(oiddict)