"""
Autheur: Samuel BOUBOUNE
Date 01/10/2021

Reste a faire:
    - Gestion des OID en dur, a voir avec liste complète des OID
"""
import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

import csv
from Hardware import Hardware
import ast

description = ".1.3.6.1.2.1.1.1.0"
interfaces = ".1.3.6.1.2.1.31.1.1.1.1"
intCurrentStatus = ".1.3.6.1.2.1.2.2.1.8"
ram = ".1.3.6.1.4.1.2021.4.6.0"
disk = ".1.3.6.1.4.1.2021.9.1.7.1"
cpu = ".1.3.6.1.4.1.2021.10.1.3.2"

class CSVConfigWriter:
    '''Cette classe permet :
    - De lire le fichier de configuration, c'est à dire, instancier une liste d'objets Hardware à partir du fichier de configuration CSV
    - D'écrire le fichier de configuration, c'est à dire écrire une liste d'objets Hardware dans le fichier de configuration.
    On réécrit la configuration en entier
    '''
    def __init__(self,config_file="Configuration_module/configuration_hardware.csv"):
        '''Instancie la liste de matériels à partir du fichier de configuration'''
        self.configuration_file="Configuration_module/configuration_hardware.csv"
        self.monitored_hardwares= self.read_hardware_csv()

    def get_configuration_file(self) -> str:
        return self.configuration_file

    def get_monitored_hardware(self) -> list[Hardware]:
        return self.monitored_hardwares
    
    def set_monitored_hardware(self, hardware_list: list[Hardware]):
        self.monitored_hardwares = hardware_list
        self.__write_hardwares_csv(self.monitored_hardwares)

    
    def read_hardware_csv(self) -> list[Hardware]:
        '''Cette méthode lit le fichier de configuration et renvoie une liste d'objet Hardware '''
        monitored_hardware = self.__csvConverter()
        monitored_hardware = self.__create_Hardwares(monitored_hardware) #On transforme la liste de listes en liste de Hardware 

        return monitored_hardware
    
    def __write_hardwares_csv(self,hardware_list: list[Hardware]):
        """Écrit la liste de hardware actuelle dans le fichier CSV
        En principe cette méthode est à appeler directement après la modification de la liste"""
        rows = list()
        firstrow=["ipaddress","name","community","oid_dict_list"]
        rows.append(firstrow)
        for hardware in hardware_list:
            assert isinstance(hardware,Hardware)
            # data rows of csv file
            row = [hardware.ip_address, hardware.name, hardware.community, hardware.oid_dict_list]
            rows.append(row)

            # writing to csv file
        with open(self.configuration_file, "w", newline="", encoding="utf-8") as csvfile:
            # creating a csv writer object
            csvwriter = csv.writer(csvfile, delimiter=";")
            # writing the data rows
            csvwriter.writerows(rows)
            

    
    def __csvConverter(self) -> list[list[str]]:
        '''Méthode qui lit chaque ligne du CSV et renvoie une liste de listes de string contenant les caractéristiques d'un matériel'''
        monitored_hardware = [] #Contient les infos du CSV sous forme de liste de liste avec les infos du hardware
        #print("\n")
        #print("Adresse IP - Nom - Communauté - Liste des OID - Request Period")
        with open(self.get_configuration_file(), newline="", mode='r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=";", quotechar="|")
            for row in spamreader:
                #print(" - ".join(row))
                monitored_hardware.append(row)
        return monitored_hardware

    def __create_Hardwares(self,hards) -> list[Hardware]:
        '''Cette méthode prend une liste de listes de string contenant les caractéristiques d'un matériel et renvoie une liste d'objets Hardware '''
        hardware_list=[]

        #ATTENTION SI LE FICHIER CSV CONTIENT UNE LIGNE VIDE A LA FIN UNE ERREUR SE PRODUIRA !!!
        for hard in hards[1:]: #hard[1:] Permet d'exclure la première ligne qui correspond à la description du csv
            ip_address = hard[0]
            name = hard[1]
            community = hard[2]
            oid_list_str = hard[3]
            oid_dict_list = ast.literal_eval(oid_list_str)
            hardware = Hardware(ip_address, name, oid_dict_list, community)
            hardware_list.append(hardware)
        return hardware_list

    # def __get_oid_dict_list(self, oid_list_of_list: list[list]) ->list[dict] :
    #     oid_dict_list = list()
    #     for oid_list in oid_list_of_list:
    #         oid_dict = dict()
    #         oid_dict["oid"]=oid_list[0]
    #         oid_dict["description"]=oid_list[1]
    #         oid_dict["req_time"]=str(oid_list[2])
    #         oid_dict_list.append(oid_dict)
    #     return oid_dict_list

        pass


    # création de la liste des OID pour une machine
    def oidListCreator():
        ''' Cette méthode ne va pas ici !
        Méthode à priori plus utilisée'''
        # creating an empty list
        oidList = []

        # number of elements as input
        oidNumber = int(input("Entrez le nombre d'éléments à superviser: "))

        print(
            "\nEntrez le chiffre correspondant a l'élément a superviser puis faites ENTRER:"
        )
        print("1 : description")
        print("2 : interfaces")
        print("3 : statut des interfaces")
        print("4 : ram")
        print("5 : disques")
        print("6 : cpu")

        # iterating till the range
        for i in range(1, oidNumber + 1):
            oid = int(input("\nElément n° %s:" % (i)))
            if oid == 1:
                oidList.append(description)
            elif oid == 2:
                oidList.append(interfaces)
            elif oid == 3:
                oidList.append(intCurrentStatus)
            elif oid == 4:
                oidList.append(ram)
            elif oid == 5:
                oidList.append(disk)
            elif oid == 6:
                oidList.append(cpu)

        return oidList

    

    