"""
Autheur: Samuel BOUBOUNE
Date 01/10/2021

Tests unitaires
"""
import sys
import os

from UserInterface import UserInterface
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
print(os.listdir())

from CSVConfigWriter import CSVConfigWriter
from ConfigurationManager import ConfigurationManager
from Hardware import Hardware
import pytest
#####################
## CSVConfigWriter ##
#####################
class TestCSVConfigWriter:
    def test_read_hardware_csv(self):
        self.writer = CSVConfigWriter()
        liste_hardware = self.writer.read_hardware_csv()
        assert type(liste_hardware) == list
        assert type(liste_hardware[0]) == Hardware

    
##########################
## ConfigurationManager ##
##########################

class TestConfigurationManager:
    pass

class TestUserInterface:
    pass




###############################
### TESTS MODULE COMPLET ###
###############################

def config_manager():
    manager = UserInterface()
    hardware_list = manager.getMonitoredHardware()
    print(hardware_list)
    print(len(hardware_list))
    manager.affichage()

if __name__ == "__main__":
    config_manager()