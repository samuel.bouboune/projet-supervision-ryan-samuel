"""
Auteur: Samuel BOUBOUNE
Date 01/10/2021

Reste a faire:
    - Gestion des OID en dur, a voir avec liste complète des OID
    - Liste des objets OK
    - Creer les fonctions get et del
"""

# imports
from Configuration_module.CSVConfigWriter import CSVConfigWriter
from Hardware import Hardware

# classe ConfigurationManager
class ConfigurationManager:
    """Classe pour gérer la configuration des équipements à superviser"""

    def __init__(self, config_file="Configuration_module/configuration_hardware.csv"):
        self.csv_writer = CSVConfigWriter(config_file)
        self.configuration_file = config_file
        self.monitored_hardware = self.csv_writer.read_hardware_csv()

    def getMonitoredHardware(self) -> list[Hardware]:
        """Retourne la liste des équipenents supervisés"""
        return self.monitored_hardware

    # complète le fichier de configuration principale
    def setConfigurationFile(self, configuration_file):
        """Change l'emplacement du fichier de configuration
        Déplacement de l'ancien fichier à implémenter """
        self.csv_writer.configuration_file = configuration_file
    
    def addHardware(self, newhardware: Hardware) -> Hardware:
        """Ajoute un matériel à la liste et s'assure qu'il soit écrit dans le csv"""
        try:
            exists = self.check_if_hardware_exists(newhardware)
            if exists:
                raise Exception('Le matériel existe déjà dans la liste !')
            elif not isinstance(newhardware, Hardware):
                return None
            else:
                self.monitored_hardware.append(newhardware)
                self.csv_writer.set_monitored_hardware(self.monitored_hardware)
                return newhardware
        except Exception as e:
            print(e)
            return None
        #Ici, écrire le matériel dans le CSV !!

    def delHardware(self, ip) -> Hardware:
        """Supprime un matériel de la liste de hardware
        Supprime un matériel du fichier CSV"""
        hardware_to_delete = self.getHardware(ip) #Retrouve le matériel ayant l'adresse IP spécifiée, s'il existe
        try:
            self.monitored_hardware.remove(hardware_to_delete)
            self.csv_writer.set_monitored_hardware(self.monitored_hardware)
            return hardware_to_delete
        except Exception as e:
            print("Le matériel à supprimer n'existe pas !")
            return None

    def modifyHardware(self, oldhardware_ip: str, newHardware: Hardware):
        """Modifie un matériel dans la liste et s'assure que le csv soit modifié"""
        oldhardware = self.getHardware(oldhardware_ip)

        if oldhardware == newHardware:
            return None, None

        index= self.monitored_hardware.index(oldhardware)
        self.monitored_hardware[index]=newHardware
        self.csv_writer.set_monitored_hardware(self.monitored_hardware)
        return oldhardware, newHardware

    def getHardware(self, ip_address) -> Hardware:
        """ Renvoie l'objet Hardware correspondant à l'adresse ip indiquée"""
        hardware_list = self.getMonitoredHardware()
        for hardware in hardware_list:
            if(hardware.ip_address == ip_address):
                return hardware
        return None
    
    def check_if_hardware_exists(self, hardware: Hardware) -> bool:
        """Vérifie si un matériel existe déjà dans la liste"""
        for hard in self.monitored_hardware:
            if hard.ip_address == hardware.ip_address:
                return True
        return False
  
# def main():
#     pass
        
        
    
# if __name__ == "__main__":
#     main()