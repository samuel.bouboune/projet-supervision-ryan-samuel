"""
Autheur: Samuel BOUBOUNE
Date 15/10/2021

"""

# imports
from pysnmp.proto.rfc1902 import IpAddress
from Configuration_module.CSVConfigWriter import CSVConfigWriter
from Configuration_module.ConfigurationManager import ConfigurationManager
from Hardware import Hardware, ModifyHardware
import socket 

class UserInterface(ConfigurationManager):
    
    def __init__(self):
        super().__init__()     
         
    def affichage(self):
        """ Mode "run" de l'interface utilisateur du ConfigurationManager""" 
        print("Bienvenue dans notre application de supervision !\n")
        while True:  
            config_option = str(input("\nSaisir les caractères correspondant pour effectuer les actions suivantes" +
                                    "\n\"1\" ==> Ajouter des materiels à superviser" +
                                    "\n\"2\" ==> Modifier des materiels à superviser" +
                                    "\n\"3\" ==> Supprimer des materiels à superviser" +
                                    "\n\"4\" ==> Afficher les materiels enregistrés" +
                                    "\n\"r\" ==> Retour" +
                                    "\n\n                                    Choix: "))
            if config_option == "1" :
                self.addHardware()
            if config_option == "2" :
                self.modifyHardware()
            if config_option == "3" :
                self.delHardware()
            if config_option == "4" :
                for hardware in self.monitored_hardware:
                    print(hardware)
            if config_option.lower() == "r" :
                break

     # création et écriture du fichier de config csv
    def addHardware(self):
        """Demande à l'utilisateur d'ajouter un matériel et l'ajoute dans la configuration"""
        new_hardware = self.__ask_user_for_new_hardware()
        return super().addHardware(new_hardware)

    def __ask_user_for_new_hardware(self) -> Hardware:
        """Demande à l'utilisateur les paramètres d'un matériel à ajouter
        et renvoie un objet Hardware """

        ipaddr = input("\nEntrez l'adresse ipaddr de la machine a inscrire: ")
        testip = UserInterface.testformatip(ipaddr)
        while not testip:
            ipaddr = input(
                "Format incorect !\nEntrez une IP au format suivant: <1-254>.<0-254>.<0-254>.<0-254>\nAdresse IP: "
            )
            testip = UserInterface.testformatip(ipaddr)

        name = str(input("Entrez le nom de la machine: "))
        community = str(input("Entrez la communauté SNMP: "))
        oiddictlist = UserInterface.__ask_oid_dict_list()

        return Hardware(ipaddr,name,oiddictlist,community)

    def delHardware(self):
        """Demande à l'utilisateur de supprimer un matériel et l'ajoute dans la configuration"""
        ip = self.__ask_user_for_del_hardware()
        if ip == "q":
            return None
        else:
            return super().delHardware(ip)

    def __ask_user_for_del_hardware(self):
        ip_addr = str(input("Entrer l'adresse IP du matériel à supprimer  ou q pour quitter: "))
        while (not UserInterface.testformatip(ip_addr)) and (not ip_addr == "q"):
            ip_addr = str(input("Adresse IP¨incorrecte ! réessayer ou q pour quitter"))

        return ip_addr
    
    def modifyHardware(self):
        """Demande à l'utilisateur de modifier un matériel et l'ajoute dans la configuration"""
        from Hardware import ModifyHardware
        oldhardware_ip, modified_hardware = self.ask_user_modify_hardware()
        return super().modifyHardware(oldhardware_ip, modified_hardware)
    
    def ask_user_modify_hardware(self):
        """Interface de modification d'un matériel"""
        ip_addr = str(input("Entrer l'adresse IP du matériel à modifier : "))
        old_hardware=self.getHardware(ip_addr)
        modified_hardware = ModifyHardware(old_hardware)
        notfinished = True
        while notfinished:  
                    config_option = str(input("\nSaisir le caractère correspondant pour effectuer les actions suivantes" +
                                            "\n\"1\" ==> Modifier l'adresse IP" +
                                            "\n\"2\" ==> Modifier le nom" +
                                            "\n\"3\" ==> Modifier les OID à superviser" +
                                            "\n\"4\" ==> Modifier la communauté" +
                                            "\n\"r\" ==> Retour" +
                                            "\n\n                                    Choix: "))
                    if config_option == "1" :
                        modified_hardware.modifyIPAddress()
                    if config_option == "2" :
                        modified_hardware.modifyName()
                    if config_option == "3" :
                        modified_hardware.modifyOidListDict()
                    if config_option == "4" :
                        modified_hardware.modifyCommunity()
                    if config_option.lower() == "r" :
                        notfinished = False
                        break
        return ip_addr, modified_hardware

    def __ask_oid_dict_list() -> list[dict]:
        """Demande la liste de dictionnaires d'oid à l'utilisateur"""
        number_of_elements = int(input("Combien d'éléments voulez vous superviser ?"))
        oid_dict_list = list()
        for i in range(number_of_elements):
            print("Élément numéro "+ str(i) + " :")
            oid_dict = UserInterface.ask_oid_dict()
            oid_dict_list.append(oid_dict)
        return oid_dict_list
    
    def ask_oid_dict() -> dict:
        """Demande un dictionnaire d'oid à l'utilisateur"""
        oid_dict = dict()
        oid_dict["oid"] = str(input("Entrez un OID : "))
        oid_dict["description"] = str(input("Entrez une description de l'OID : "))
        oid_dict["req_time"] = str(input("Entrez un temps de requêtage : "))
        print("")
        return oid_dict


    # Test si l'ip saisie par l'utilisateur a un format correct
    def testformatip(ip):
        # '''Pas à mettre ici'''
        # if ip == "8.8.8.8":
        #     return True
        # elif ip == "127.0.0.1":
        #     return True
        # elif len(ip) < 7 or len(ip) > 15:
        #     return False
        # elif ip.count(".") != 3:
        #     return False
        # elif ip.count(".") == 3:
        #     octets = ip.split(".")
        #     for i in range(len(octets) - 1):
        #         try:
        #             if int(octets[i]) > 255 or int(octets[i]) < 0:
        #                 return False
        #         except ValueError:
        #             return False
        #     if octets[0] == "10":
        #         return True
        #     elif octets[1] == "168":
        #         if octets[0] == "192":
        #             return True
        #         else:
        #             return False
        #     elif octets[1] == "16":
        #         if octets[0] == "172":
        #             return True
        #         else:
        #             return False
        try:
            socket.inet_aton(ip)
            return True
        except socket.error:
            if UserInterface.is_valid_hostname(ip):
                return True
            else:
                return False
        

    def is_valid_hostname(hostname):
        import re
        if len(hostname) > 255:
            return False
        if hostname[-1] == ".":
            hostname = hostname[:-1] # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in hostname.split("."))       


def main():
    interface = UserInterface()
    interface.affichage()
        
if __name__ == "__main__":
    main()