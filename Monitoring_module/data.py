
class Data:
    """Classe utilisée pour stocker les données liées à une requête SNMP
    et pour écrire dans le fichier de données CSV """
    def __init__(self,ip_address: str, timestamp: str,value: str,oid: str, description: str) -> None:
        self.ip_address = ip_address
        self.timestamp = timestamp
        self.value = value
        self.oid = oid
        self.description= description
        self.folder = "data/" + ip_address + "/"
        self.datafilename = self.folder + oid + ".csv"
    
    def get_ip_address(self):
        return self.ip_address

    def get_timestamp(self):
        return self.timestamp

    def get_value(self):
        return self.value 

    def get_oid(self):
        return self.oid

    def get_folder(self):
        return self.folder
    
    def get_datafilename(self):
        return self.datafilename

    def get_description(self):
        return self.description

    def get_values_for_csv(self) -> list :
        """Retourne une liste contenant les données à écrire dans le CSV dans le bon ordre"""
        donnees = [ self.timestamp, self.description,self.ip_address, self.value, self.oid ]
        return donnees
        
    

def test_data():
    data = Data("192.168.1.1","timestamp","valeur","oid", "description")
    print("Nom de fichier : "+ data.get_datafilename())
    print(data.get_values_for_csv())
    return data
     
