
"""
Auteur : Ryan BADAÏ

Prérequis : 
csv

Description :
Cette classe CSVData_Writer sert à écrire les données requêtées dans un fichier CSV 

To-Do : 
- Implémenter write_data()
"""
import csv
from Monitoring_module.data import Data
import os

class CSVData_Writer:
    """Classe qui gère l'écriture des données issues de la supervision"""
    def __init__(self):
        pass

    #Faire un fichier par OID 
    def write_data_row(self, data: Data) -> None: 
        """Écrit un objet de type data dans le bon fichier"""
        donnees = data.get_values_for_csv()
        dir = data.get_folder()
        self.__check_dir(dir)
        datafilename = data.get_datafilename()
        self.__check_file(datafilename)

        with open(datafilename, 'a', encoding='UTF8',) as f:
            writer = csv.writer(f, lineterminator = '\n')
            # write the data
            writer.writerow(donnees)

    def __check_dir(self,dir_name: str) -> None :
        """Vérifie l'existence du dossier où les data seront stockées"""
        directory = os.path.dirname(dir_name)
        if not os.path.exists(directory):
            os.makedirs(directory)

    def __check_file(self,filename: str) -> None:
        """Vérifie l'existence du fichier csv"""
        file = os.path.dirname(filename)
        if not os.path.exists(filename):
            with open(filename, 'a', encoding='UTF8',) as f:
                writer = csv.writer(f, lineterminator = '\n')
                entete = ["Timestamp", "Description","Adresse IP", "Valeur", "OID"]
                # write the data
                writer.writerow(entete)

    