"""
Auteur : Ryan BADAÏ

Prérequis :
pip install pytest
import os
SNMPpoller
Hardware
CSVData_Writer


Description :
Ce fichier est un test unitaire pour la classe SNMPpoller. Il faut utiliser la librairie pytest.

Il faut, dans un powershell, se placer dans le dossier où se trouvent
les fichiers de test commançant par test_
Commande à lancer pour tester : python -m pytest -s

LE "-s" permet d'afficher le print(result)
"""
#Ajout du dossier parent pour le scan des packages
import sys
import os
current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)
from Hardware import Hardware
from SNMPpoller import SNMPpoller
from CSVData_Writer import CSVData_Writer
from Monitoring_Manager import Monitoring_Manager
from Monitoring_module.data import Data

###########################
# TESTS UNITAIRES
###########################
#---------- TESTS SNMPpoller ------------------------
def test_get_data():
    poller = SNMPpoller()
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="10"
    data=poller.get_data("mau-nas1-176-2.local.univ-savoie.fr", oid1, "passprojet")
    assert isinstance(data, Data)
#------------- TESTS Hardware.py -----------------------
def test_init():
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="10"
    liste_oid = list()
    liste_oid.append(oid1)
    hard = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    return hard

def test_eq():
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="10"
    liste_oid = list()
    liste_oid.append(oid1)
    hard = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    hard2 = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    hard3 = Hardware("TEST","NAS", liste_oid, "passprojet")
    assert(hard==hard2)
    assert(hard!=hard3)

#--------------- TESTS CSVData_Writer.py -----------------
def test_write_csv():
    writer = CSVData_Writer()
    data = Data("192.168.1.1","timestamp","valeur","oid","Description")
    writer.write_data_row(data)
    #os.remove("testdata.csv")

def test_pass():
    pass

#--------------- TESTS data.py -----------------
def test_get_values_for_csv():
    data = Data("192.168.1.1","timestamp","valeur","oid","Description")
    values = data.get_values_for_csv()
    assert type(values) == list
    assert len(values) == 5

#--------------- TESTS MonitoringManager -----------------

def get_example_Hardware_list():
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="5"
    liste_oid = list()
    liste_oid.append(oid1)
    hard = Hardware("hard1","hard1", liste_oid, "passprojet")
    hard2 = Hardware("hard2","hard2", liste_oid, "passprojet")
    hard3 = Hardware("hard3","hard3", liste_oid, "passprojet")
    hardwares =[]
    hardwares.append(hard)
    hardwares.append(hard2)
    hardwares.append(hard3)
    return hardwares

def test_add_Hardware():
    hardlist = get_example_Hardware_list()
    monit_manager = Monitoring_Manager(hardlist)
    monit_manager.addhardware(hardlist[2])
    assert len(monit_manager.threads)==4
    assert(len(monit_manager.monitored_hardware)==4)
    monit_manager.stop()

def test_remove_Hardware():
    hardlist = get_example_Hardware_list()
    monit_manager = Monitoring_Manager(hardlist)
    monit_manager.removehardware(hardlist[2])
    assert len(monit_manager.threads) == 2
    assert len(monit_manager.monitored_hardware) == 2
    monit_manager.stop()

def test_modify_hardware():
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="5"
    liste_oid = list()
    liste_oid.append(oid1)
    hardlist = get_example_Hardware_list()
    monit_manager = Monitoring_Manager(hardlist)
    oldhardware = hardlist[2]
    newhardware = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    monit_manager.modifyhardware(oldhardware,newhardware)
    assert newhardware in monit_manager.monitored_hardware
    assert oldhardware not in monit_manager.monitored_hardware
    monit_manager.stop()

    


###########################
# TESTS DU MODULE COMPLET 
###########################

def monitoring_manager():
    """Pas possible / difficile à tester automatiquement, il faut 
    suivre le déroulement au débugueur"""
    oid1 = dict()
    oid1["oid"]="1.3.6.1.4.1.2021.10.1.5.1"
    oid1["description"]="Description de l'oid"
    oid1["req_time"]="5"
    liste_oid = list()
    liste_oid.append(oid1)
    hard = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    hard2 = Hardware("mau-nas1-176-2.local.univ-savoie.fr","NAS", liste_oid, "passprojet")
    hardwares =[]
    hardwares.append(hard)
    hardwares.append(hard2)
    monit_manager = Monitoring_Manager(hardwares)


if __name__ == "__main__":
    monitoring_manager()
    #test_get_data()
    #test_add_Hardware()
    #test_remove_Hardware()


