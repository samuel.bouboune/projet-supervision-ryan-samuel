import threading
from Monitoring_module.CSVData_Writer import CSVData_Writer
from Monitoring_module.SNMPpoller import SNMPpoller
from Hardware import Hardware
import time

class OIDThread(threading.Thread):
    """Cette classe est un thread de surveillance d'un seul OID"""

    def __init__(self, hardware: Hardware, oid_dict: dict, csvwriter: CSVData_Writer, poller: SNMPpoller) -> None:
        """
        L'oid_dict est un dictionnaire contenant l'oid (str), la description (str)
        et le temps de requêtage req_time (str)
        """

        #Les assert permettent de préciser le type des objets pour l'autocomplétion 
        assert isinstance(csvwriter, CSVData_Writer)
        assert isinstance(poller, SNMPpoller)
        assert isinstance(hardware,Hardware)

        threading.Thread.__init__(self)
        self.ip_address = hardware.getIPAaddress()
        self.community = hardware.getCommunity()
        self.hardware = hardware
        self.oid_dict = oid_dict
        self.oid = oid_dict["oid"]
        self.req_time = float(oid_dict["req_time"])
        self.description = oid_dict["description"]
        self.exists = True
        self.poller = poller
        self.csvwriter = csvwriter
        
        #self.run()

    def run(self):
        """Surveille l'oid selon la période de requêtage demandée par l'utilisateur"""
        while(self.exists):
            try:
                data = self.poller.get_data(self.ip_address, self.oid_dict,self.community)
                self.csvwriter.write_data_row(data)
            except Exception as e :
                print(e)

            time.sleep(self.req_time)

    def kill(self) -> None:
        """Met fin au thread de surveillance une fois le sleep terminé"""
        self.exists = False

    def does_exists(self):
        """ Permet de voir si le thread est en cours d'exécution"""
        return self.exists