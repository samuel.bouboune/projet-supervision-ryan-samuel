"""
Auteur : Ryan BADAÏ

Prérequis : 
Hardware.py

Description :
Cette classe représente le module de supervision dans sa globalité
Il regroupe l'ensemble des sous-modules 

To-Do : 
- Implémenter la méthode run :
    --> D'abord implémenter la classe CSVData_Writer 


"""
from Monitoring_module.OIDThread import OIDThread
from Monitoring_module.SNMPpoller import SNMPpoller
from Monitoring_module.CSVData_Writer import CSVData_Writer
from Hardware import Hardware

AJOUT=0
SUPPRESSION=1
MODIFICATION=2


class Monitoring_Manager():
    """ Cette classe gère les aspects de la supervision
    Fournir une liste de matériels pour l'instancier"""

    def __init__(self,monitored_hardware: list[Hardware]):
        """Attention la liste de matériels est une référence vers celle du config manager """
        self.monitored_hardware = list()
        self.monitored_hardware = monitored_hardware
        self.poller = SNMPpoller()
        self.csvmanager = CSVData_Writer()
        self.threads = list()
        self.__init_hardwares_threads()

    def get_monitored_hardware(self) -> list[Hardware]:
        """Retourne la liste de matériels supervisés"""
        return self.monitored_hardware

    def __init_hardwares_threads(self) -> None:
        """
        Cette méthode parcourt la liste du matériel fournie lors de l'initialisation 
        et crée des threads de surveillance pour chaque OID à monitorer.
        """
        for hardware in self.monitored_hardware:
            self.__init_thread(hardware)    

    def __init_thread(self, hardware: Hardware) -> None:
        """Crée les threads de surveillance associés à un matériel"""
        assert isinstance(hardware, Hardware)
        for oid in hardware.getOids():
            thread = OIDThread(hardware, oid, self.csvmanager, self.poller)
            #thread.run() #Pour débuguer uniquement !!!
            thread.start()
            self.threads.append(thread)

    def addhardware(self, hardware: Hardware) -> None:
        """Ajoute un matériel passé en paramètre et crée les threads de surveillance associés"""
        self.__init_thread(hardware)

    def removehardware(self, hardware: Hardware) -> None:
        """Supprime un matériel passé en paramètre (et arrête les threads associés)"""
        for thread in self.threads:
            assert isinstance(thread,OIDThread)
            if thread.hardware == hardware:
                thread.kill()
                self.threads.remove(thread)

    def modifyhardware(self, oldhardware: Hardware, newhardware: Hardware) -> None:
        """Modifie un matériel, càd :
        Supprime l'ancien matériel et ajoute le nouveau à la place"""
        self.removehardware(oldhardware)
        self.addhardware(newhardware)

    def stop(self) -> None:
        """Arrête tous les threads en cours pour terminer le programme"""
        for thread in self.threads:
            assert isinstance(thread,OIDThread)
            thread.kill()
