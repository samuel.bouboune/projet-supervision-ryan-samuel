"""
Auteur : Ryan BADAÏ

Prérequis : 
pip install pysnmp 

Description : 
Cette classe est une interface pour utiliser la librairie pysnmp de façon simple 

"""
import pysnmp
from pysnmp.hlapi import *

from pysnmp import hlapi

from datetime import datetime, time
from Monitoring_module.data import Data



class SNMPpoller:
    """Classe permettant de gérer les requêtes SNMP"""
    def __init__(self) -> None:
        pass
    
    # snmp_request(string,string,string)
    def __snmp_request(self, ip_adress: str, oid: str, community: str) -> str:
        """ Méthode privée
        Fait une requête SNMP et renvoie uniquement le résultat"""
        # Définition d'une communauté
        cred = hlapi.CommunityData(community)

        # La fonction get prend une liste d'oid comme paramètre. Ici on transforme l'unique oid en liste pour que ça marche
        oidlist = list()
        oidlist.append(oid)

        # Requête et retour de la valeur sous forme de string
        try:
            result = get(ip_adress, oidlist, cred)
            # Extraction de la valeur
            valeurs = list(result.values())
            valeur = valeurs[0]
            valeur = str(valeur)
            return valeur
        except ValueError:  # Exception qui sera signalée
            print("Erreur dans les paramètres")
            return "Erreur"

    def get_data(self, ip_adress: str, oid_dict: dict(), community: str) -> Data:
        """Fait une requête SNMP et renvoie un objet Data"""
        timestamp = datetime.now()
        timestamp = timestamp.strftime("%d/%m/%Y %H:%M:%S")
        value = self.__snmp_request(ip_adress,oid_dict["oid"],community)
        data=Data(ip_adress,timestamp, value, oid_dict["oid"], oid_dict["description"])
        return data

"""
Définition des fonctions utilisées dans la classe
Copié collé d'internet, inutile de lire, dur à comprendre sans documentation 
"""


def get(
    target,
    oids,
    credentials,
    port=161,
    engine=hlapi.SnmpEngine(),
    context=hlapi.ContextData(),
):
    handler = hlapi.getCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_object_types(oids)
    )
    return fetch(handler, 1)[0]


def construct_object_types(list_of_oids):
    object_types = []
    for oid in list_of_oids:
        object_types.append(hlapi.ObjectType(hlapi.ObjectIdentity(oid)))
    return object_types


def fetch(handler, count):
    result = []
    for i in range(count):
        try:
            error_indication, error_status, error_index, var_binds = next(handler)
            if not error_indication and not error_status:
                items = {}
                for var_bind in var_binds:
                    items[str(var_bind[0])] = cast(var_bind[1])
                result.append(items)
            else:
                raise RuntimeError("Got SNMP error: {0}".format(error_indication))
        except StopIteration:
            break
    return result


def cast(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        try:
            return float(value)
        except (ValueError, TypeError):
            try:
                return str(value)
            except (ValueError, TypeError):
                pass
    return value
