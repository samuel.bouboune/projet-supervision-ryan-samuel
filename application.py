from ObservableUserInterface import ObservableUserInterface
from ObserverMonitoringManager import ObserverMonitoringManager
from DataViewer_module.DataViewer import DataViewerV2

class Application:
    def __init__(self) -> None:
        #Création du config manager 
        self.uiconfigmanager = ObservableUserInterface()
        
       #Création du Monitoring_Manager
        self.hardware_list = self.uiconfigmanager.getMonitoredHardware() #ATTENTION : passage par référence !!
        self.monit_manager = ObserverMonitoringManager(self.hardware_list)

        #Abonnement 
        self.uiconfigmanager.registerObserver(self.monit_manager)

    def run(self) -> None:
        self.uiconfigmanager.affichage()
        self.monit_manager.stop()

class ApplicationV2(Application):
    """Application finale, avec le module dataviewer intégré"""
    def __init__(self) -> None:
        super().__init__()
        self.hardware_list = self.uiconfigmanager.getMonitoredHardware() #ATTENTION : passage par référence !!
        self.dataviewer = DataViewerV2(self.hardware_list)

    def run(self):
        """Interface utilisateur initiale du programme"""
        while True:
            option = str(input("\nSaisir les caractères correspondant pour effectuer les actions suivantes" +
                                                "\n\"1\" ==> Configurer la liste de matériels à superviser" +
                                                "\n\"2\" ==> Visualiser un graphe de données" +
                                                "\n\"q\" ==> Quitter l'application" +
                                                "\n\n                                    Choix: "))
            if option == "1":
                self.uiconfigmanager.affichage()
            if option == "2":
                self.dataviewer.interface()
            if option == "q":
                self.monit_manager.stop()
                break


if __name__ == "__main__":
    app = ApplicationV2()
    app.run()

